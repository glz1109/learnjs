

## JavaScript 输出

------

JavaScript 没有任何打印或者输出的函数。

### JavaScript 显示数据

JavaScript 可以通过不同的方式来输出数据：

- 使用 **window.alert()** 弹出警告框。

- 使用 **document.write()** 方法将内容写到 HTML 文档中。

  请使用 document.write() 仅仅向文档输出写内容。

  如果在文档已完成加载后执行 document.write，整个 HTML 页面将被覆盖。

- 使用 **innerHTML** 写入到 HTML 元素。
  
  如需从 JavaScript 访问某个 HTML 元素，您可以使用 document.getElementById(*id*) 方法。
  
  请使用 "id" 属性来标识 HTML 元素，并 innerHTML 来获取或插入元素内容：
  
- 使用 **console.log()** 写入到浏览器的控制台。

  如果您的浏览器支持调试，你可以使用 **console.log()** 方法在浏览器中显示 JavaScript 值。

  浏览器中使用 F12 来启用调试模式， 在调试窗口中点击 "Console" 菜单。

  console.log()的主要是方便调试javascript, 可以看到在页面中输出的内容。

  相比alert的优点：

  能看到结构化的东西，如果是alert，弹出一个对象就是[object object],但是console能看到对象的内容。

  console不会打断页面的操作，如果用alert弹出来内容，那么页面就死了，但是console输出内容后页面还可以正常操作。




## JavaScript 弱类型

JavaScript是弱类型编程语言,定义变量都使用 var 定义,与 Java 这种强类型语言有区别.

在定义后可以通过 **typeOf()** 来获取JavaScript中变量的数据类型.

// Number 通过数字字面量赋值 

 // Number 通过表达式字面量赋值

// String 通过字符串字面量赋值

// Array 通过数组字面量赋值 

// Object 通过对象字面量赋值

有个情况需要特别注意: **typeof 不能用来判断是 Array 还是Object**

var arr = [] typeof(arr) === 'object' // true

结果为 **true**。

当然你可以使用其他方式来判断：

**1、使用 isArray 方法**

```javascript
var cars=new Array();
cars[0]="Saab";
cars[1]="Volvo";
cars[2]="BMW";
// 判断是否支持该方法
if (Array.isArray) {
    if(Array.isArray(cars)) {
        document.write("该对象是一个数组。") ;
    }
}
```

2、使用 instanceof 操作符

```javascript
var cars=new Array();
cars[0]="Saab";
cars[1]="Volvo";
cars[2]="BMW";

if (cars instanceof Array) {
    document.write("该对象是一个数组。") ;
}
```

更多内容可以参考：

-  [JavaScript 判断对象是否为数组](https://www.runoob.com/note/26685)
-  [JavaScript 判断该对象是否为数组](https://www.runoob.com/w3cnote/javascript-check-arrayisobject.html)



## JavaScript 变量

### 命令规则

与代数一样，JavaScript 变量可用于存放值（比如 x=5）和表达式（比如 z=x+y）。

变量可以使用短名称（比如 x 和 y），也可以使用描述性更好的名称（比如 age, sum, totalvolume）。

- 变量必须以字母开头

- 变量也能以 $ 和 _ 符号开头（不过我们不推荐这么做）

- 变量名称对大小写敏感（y 和 Y 是不同的变量）

  

  如果重新声明 JavaScript 变量，该变量的值不会丢失：

  在以下两条语句执行后，变量 carname 的值依然是 "Volvo"：
```javascript
var carname="Volvo";
var carname;
```
  使用 var 关键字重新声明变量可能会带来问题。

  在块中重新声明变量也会重新声明块外的变量：

  ```javascript
var x = 10; // 这里输出 x 为 10 
{     
	var x = 2;    // 这里输出 x 为 2
} 
// 这里输出 x 为 2
  ```

let 关键字就可以解决这个问题，因为它只在 let 命令所在的代码块 **{}** 内有效。

```javascript
var x = 10; // 这里输出 x 为 10 
{     
	let x = 2;    // 这里输出 x 为 2 
} // 这里输出 x 为 10
```

### 全局变量

在函数体外或代码块外使用 **var** 和 **let** 关键字声明的变量也有点类似。

它们的作用域都是 **全局的**

### HTML 代码中使用全局变量

在 JavaScript 中, 全局作用域是针对 JavaScript 环境。

在 HTML 中, 全局作用域是针对 window 对象。

使用 **var** 关键字声明的全局作用域变量属于 window 对象:

```javascript
var carName = "Volvo"; // 可以使用 window.carName 访问变量
let carName = "Volvo"; // 不能使用 window.carName 访问变量
```

### 声明提升

JavaScript 中，函数及变量的声明都将被提升到函数的最顶部。

JavaScript 中，变量可以在使用后声明，也就是变量可以先使用再声明。

var 关键字定义的变量可以在使用后声明，也就是变量可以先使用再声明

let 关键字定义的变量则不可以在使用后声明，也就是变量需要先声明再使用。

### const 关键字

const 用于声明一个或多个常量，声明时必须进行初始化，且初始化后值不可再修改

`onst`定义常量与使用`let` 定义的变量相似：

- 二者都是块级作用域
- 都不能和它所在作用域内的其他变量或函数拥有相同的名称

两者还有以下两点区别：

- `const`声明的常量必须初始化，而`let`声明的变量不用
- const 定义常量的值不能通过再赋值修改，也不能再次声明。而 let 定义的变量值可以修改。



const 的本质: const 定义的变量并非常量，并非不可变，它定义了一个常量引用一个值。使用 const 定义的对象或者数组，其实是可变的。下面的代码并不会报错：

```javascript
// 创建常量对象 
const car = {type:"Fiat", model:"500", color:"white"};  

// 修改属性
car.color = "red";  

// 添加属性
car.owner = "Johnson";
```
但是我们不能对常量对象重新赋值：
```javascript
const car = {type:"Fiat", model:"500", color:"white"}; 
car = {type:"Volvo", model:"EX60", color:"red"};    // 错误
```

以下实例修改常量数组：
```javascript
// 创建常量数组 
const cars = ["Saab", "Volvo", "BMW"];  

// 修改元素 
cars[0] = "Toyota";  

// 添加元素 
cars.push("Audi");
```
但是我们不能对常量数组重新赋值：
```javascript
const cars = ["Saab", "Volvo", "BMW"]; 
cars = ["Toyota", "Volvo", "Audi"];    // 错误
```